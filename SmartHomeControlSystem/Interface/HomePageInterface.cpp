#include "HomePageInterface.h"
#include "ui_HomePageInterface.h"

HomePageInterface::HomePageInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HomePageInterface),
    m_isPlayMusic(false)
{
    ui->setupUi(this);
    m_weather = new Weather();
    QObject::connect(m_weather,&Weather::updateWeather,this,&HomePageInterface::updateWeater);

    // 地址输入栏
    m_autocomplete = new QtMaterialAutoComplete(this);
    QObject::connect(m_autocomplete,&QtMaterialAutoComplete::itemSelected,this,&HomePageInterface::setWeather);
    QObject::connect(m_autocomplete,&QtMaterialAutoComplete::selectionChanged,this,[=]{qobject_cast<QLineEdit*>(sender())->deselect();});
    m_autocomplete->setGeometry(130,250,300,50);
    m_autocomplete->setAlignment(Qt::AlignCenter);
    m_autocomplete->setDataSource(m_weather->getAllRegions());
    m_autocomplete->setText("北京 北京");
    m_autocomplete->setStyleSheet("QWidget{background-color:#00000000;border-radius:10px;color:rgb(191,198,220);font: bold normal 32px 'Microsoft YaHei';}");

    // 更多按钮
    m_moreBewsButton = new QtMaterialFlatButton(tr("更多"),ui->m_newsWidget);
    m_moreBewsButton->setGeometry(590,10,40,40);
    m_moreBewsButton->setForegroundColor(QColor(191,198,220));
    m_moreBewsButton->setStyleSheet("QPushButton{background-color:#00000000;font:bold normal 18px '微软雅黑';color:#FFFFFF;}");
    m_moreBewsButton->setHaloVisible(false);

    // 音乐上下方进度条
    m_musicTopProgress = new QtMaterialProgress(ui->m_newsWidget);
    m_musicTopProgress->setGeometry(0,212,ui->m_newsWidget->width(),5);
    m_musicTopProgress->setBackgroundColor(QColor(63,66,76));
    m_musicTopProgress->setProgressColor(QColor(75,80,96));
    m_musicTopProgress->setEnabled(false);
    m_musicBottomProgress = new QtMaterialProgress(ui->m_newsWidget);
    m_musicBottomProgress->setGeometry(0,280,ui->m_newsWidget->width(),5);
    m_musicBottomProgress->setBackgroundColor(QColor(63,66,76));
    m_musicBottomProgress->setProgressColor(QColor(75,80,96));
    m_musicBottomProgress->setEnabled(false);
    // 音乐播放器
    m_musicImage = new QtMaterialAvatar(QImage(":/Image/HomePageInterface/NoData.png"),ui->m_musicWidget);
    m_musicInterfaceButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/Aperture.png"),ui->m_musicWidget);
    m_previousSongButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/PreviousSong.png"),ui->m_musicWidget);
    m_suspendButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/Play.png"),ui->m_musicWidget);
    m_nextSongButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/NextSong.png"),ui->m_musicWidget);
    m_scrollFont = new ScrollFont(ui->m_musicWidget);
    // connect(m_previousSongButton,&QtMaterialIconButton::released,this,[=]{QtConcurrent::run(m_musicPlayer,&MusicPlayer::switchMusic,SwitchMusicState::PreviousSong);});
    QObject::connect(m_previousSongButton,&QtMaterialIconButton::released,this,[=]{m_musicPlayer->switchMusicState(SwitchMusicState::PreviousSong);});
    QObject::connect(m_suspendButton,&QtMaterialIconButton::released,this,[=](){m_isPlayMusic = !m_isPlayMusic;m_isPlayMusic ? m_musicPlayer->switchMusicState(SwitchMusicState::PlayCurrent) : m_musicPlayer->switchMusicState(SwitchMusicState::PausePlayback);});
    QObject::connect(m_nextSongButton,&QtMaterialIconButton::released,this,[=]{m_musicPlayer->switchMusicState(SwitchMusicState::NextSong);});
    QObject::connect(m_musicInterfaceButton,&QtMaterialFlatButton::released,this,[=]{emit switchMusicInterface();});
    m_musicImage->setGeometry(15,0,60,60);
    m_musicInterfaceButton->setGeometry(15,0,60,60);
    m_previousSongButton->setGeometry(415,5,50,50);
    m_suspendButton->setGeometry(485,5,50,50);
    m_nextSongButton->setGeometry(555,5,50,50);
    m_scrollFont->setGeometry(95,0,300,60);
    m_musicImage->setSize(60);
    m_musicInterfaceButton->setIconSize(QSize(60,60));
    m_previousSongButton->setIconSize(QSize(50,50));
    m_suspendButton->setIconSize(QSize(50,50));
    m_nextSongButton->setIconSize(QSize(50,50));
    m_musicInterfaceButton->setColor(QColor(191,198,220));
    m_previousSongButton->setColor(QColor(191,198,220));
    m_suspendButton->setColor(QColor(191,198,220));
    m_nextSongButton->setColor(QColor(191,198,220));
    m_scrollFont->setStyleSheet("QLabel{background-color:#00000000;}");
    m_scrollFont->setMoveWay(ScrollFont::LeftAndRight);
    m_musicPlayer = new MusicPlayer(this);
    QObject::connect(m_musicPlayer,&MusicPlayer::currentMusicInformation,this,[=](MUSIC_INFO musicINFO){m_scrollFont->setScrollFontText(musicINFO.SingerName != nullptr ? musicINFO.Name + " - " + musicINFO.SingerName : musicINFO.Name);});
    QObject::connect(m_musicPlayer,&MusicPlayer::currentLyrics,this,[=](QString lyrics){emit currentLyrics(lyrics);});
    QObject::connect(m_musicPlayer,&MusicPlayer::updateMusicImage,this,[=](){m_musicImage->setImage(QImage(QDir::currentPath() + "/music.jpg"));});
    QObject::connect(m_musicPlayer,&MusicPlayer::musicStateChanged,this,[=](QMediaPlayer::State newState){
        switch (newState) {
        case QMediaPlayer::StoppedState:
            m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Play.png"));
            m_isPlayMusic = false;
            m_musicTopProgress->setEnabled(false);
            m_musicBottomProgress->setEnabled(false);
            break;
        case QMediaPlayer::PlayingState:
            m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Suspend.png"));
            m_isPlayMusic = true;
            m_musicTopProgress->setEnabled(true);
            m_musicBottomProgress->setEnabled(true);
            break;
        case QMediaPlayer::PausedState:
            m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Play.png"));
            m_isPlayMusic = false;
            m_musicTopProgress->setEnabled(false);
            m_musicBottomProgress->setEnabled(false);
            break;
        }});
    QObject::connect(m_musicPlayer,&MusicPlayer::currentMusicPosition,this,[=](qint64 position,qint64 duration){m_musicslider->setRange(0,duration);m_musicslider->setValue(position);});
    // 音乐时间
    m_musicslider = new QtMaterialSlider(ui->m_newsWidget);
    QObject::connect(m_musicslider,&QtMaterialSlider::sliderMoved, this,[=](qint64 value){m_musicPlayer->setCurrentMusicPosition(value);});
    m_musicslider->setGeometry(0,277,ui->m_newsWidget->width(),5);
    m_musicslider->setThumbColor(QColor(191,198,220));
    QColor color(QColor(63,66,76));
    color.setAlpha(100);
    m_musicslider->setTrackColor(color);
    m_musicslider->setDisabledColor(QColor(63,66,76));
    m_musicslider->setDisabled(false);
}

HomePageInterface::~HomePageInterface()
{
    delete ui;
}

void HomePageInterface::setWeather()
{
    QStringList list = m_autocomplete->text().split(" ");
    if(list.size() == 2)
        m_weather->setWeather(list.at(0),list.at(1));
}

MusicPlayer* HomePageInterface::getMusicPlayer()
{
    return m_musicPlayer;
}

void HomePageInterface::updateWeater(QList<WEATHER_INFO> info)
{
    WEATHER_INFO weather = info.at(1);
    ui->m_outdoorValueLabel->setText(weather.low + "~" + weather.high);
    ui->m_weatherLabel->setText(weather.type);
    ui->m_indoorValueLabel->setText(weather.notice);
}
