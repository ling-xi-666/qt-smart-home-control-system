#ifndef HOMEPAGEINTERFACE_H
#define HOMEPAGEINTERFACE_H

#include <QWidget>
#include <Weather.h>
#include <MusicPlayer.h>
#include <ScrollFont.h>

#include <qtmaterialautocomplete.h>
#include <qtmaterialslider.h>
#include <qtmaterialprogress.h>
#include <qtmaterialflatbutton.h>
#include <qtmaterialiconbutton.h>
#include <qtmaterialavatar.h>

// QtConcurrent::run(m_musicPlayer,&MusicPlayer::switchMusic,SwitchMusicState::PreviousSong)
//#include <QtConcurrent/qtconcurrentrun.h>
namespace Ui {
class HomePageInterface;
}

class HomePageInterface : public QWidget
{
    Q_OBJECT

public:
    explicit HomePageInterface(QWidget *parent = nullptr);
    ~HomePageInterface();
Q_SIGNALS:
    void currentLyrics(QString lyrics);
    void switchMusicInterface();
public:
    void setWeather();
    MusicPlayer* getMusicPlayer();
private Q_SLOTS:
    void updateWeater(QList<WEATHER_INFO> info);
private:
    Ui::HomePageInterface *ui;

    QtMaterialAutoComplete *m_autocomplete;
    QtMaterialFlatButton *m_moreBewsButton;
    QtMaterialProgress  *m_musicTopProgress;
    QtMaterialProgress  *m_musicBottomProgress;
    QtMaterialSlider *m_musicslider;
    QtMaterialAvatar *m_musicImage;
    QtMaterialIconButton *m_musicInterfaceButton;
    QtMaterialIconButton *m_previousSongButton;
    QtMaterialIconButton *m_suspendButton;
    QtMaterialIconButton *m_nextSongButton;

    ScrollFont *m_scrollFont;
    Weather *m_weather;
    MusicPlayer *m_musicPlayer;
private:
    bool m_isPlayMusic;
};

#endif // HOMEPAGEINTERFACE_H
