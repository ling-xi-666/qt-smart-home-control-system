HEADERS += \
    $$PWD/HomePageInterface.h \
    $$PWD/MainInterface.h \
    $$PWD/RoomInterface.h \
    $$PWD/ScenePageInterface.h \
    $$PWD/SecurityInterface.h \
    $$PWD/MonitorInterface.h \
    $$PWD/MoreInterface.h \
    $$PWD/SubInterface/MusicInterface.h
SOURCES += \
    $$PWD/HomePageInterface.cpp \
    $$PWD/MainInterface.cpp \
    $$PWD/RoomInterface.cpp \
    $$PWD/ScenePageInterface.cpp \
    $$PWD/SecurityInterface.cpp \
    $$PWD/MonitorInterface.cpp \
    $$PWD/MoreInterface.cpp \
    $$PWD/SubInterface/MusicInterface.cpp
FORMS += \
    $$PWD/HomePageInterface.ui \
    $$PWD/MainInterface.ui \
    $$PWD/RoomInterface.ui \
    $$PWD/ScenePageInterface.ui \
    $$PWD/SecurityInterface.ui \
    $$PWD/MonitorInterface.ui \
    $$PWD/MoreInterface.ui \
    $$PWD/SubInterface/MusicInterface.ui
INCLUDEPATH += $$PWD
