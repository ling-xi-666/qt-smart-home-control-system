#include "MainInterface.h"
#include "ui_MainInterface.h"

MainInterface::MainInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainInterface),
    m_appBar(new QtMaterialAppBar(this)),
    m_drawer(new QtMaterialDrawer(this)),
    m_openListButton(new QtMaterialIconButton(QIcon(":/Image/BackgroundImage/List.png"),this)),
    m_closeListButton(new QtMaterialRaisedButton(("Return"),this)),
    //    m_testButton(new QtMaterialFlatButton(tr("维护"),this)),
    m_narrowButton(new QtMaterialIconButton(QIcon(":/Image/BackgroundImage/Narrow.png"),this)),
    m_closeButton(new QtMaterialIconButton(QIcon(":/Image/BackgroundImage/Close.png"),this)),
    m_tabs(new QtMaterialTabs(this)),
    m_dialog(new QtMaterialDialog(this)),
    m_snackbar(new QtMaterialSnackbar(this))
{
    ui->setupUi(this);

    m_homePageInterface = new HomePageInterface(this);
    m_roomInterface = new RoomInterface(this);
    m_scenePageInterface = new ScenePageInterface(this);
    m_securityInterface = new SecurityInterface(this);
    m_monitorInterface = new MonitorInterface(this);
    m_moreInterface = new MoreInterface(this);

    m_homePageInterface->setGeometry(0,50,this->width(),682);
    m_roomInterface->setGeometry(0,50,this->width(),682);
    m_scenePageInterface->setGeometry(0,50,this->width(),682);
    m_securityInterface->setGeometry(0,50,this->width(),682);
    m_monitorInterface->setGeometry(0,50,this->width(),682);
    m_moreInterface->setGeometry(0,50,this->width(),682);

    QObject::connect(m_homePageInterface,&HomePageInterface::currentLyrics,this,[=](QString lyrics){m_scrollFont->setText(lyrics);});
    QObject::connect(m_homePageInterface,&HomePageInterface::switchMusicInterface,this,[=](){m_subInterface = SubInterface::MusicInformation;m_musicInterface->setVisible(true);});


    // 导航栏
    m_timeLabel = new QLabel("智能家居控制系统");
    m_timeLabel->setAttribute(Qt::WA_TranslucentBackground);
    m_timeLabel->setForegroundRole(QPalette::WindowText);
    m_timeLabel->setContentsMargins(6, 0, 0, 0);
    m_timeLabel->setFont(QFont("Roboto", 11, QFont::Normal));
    QPalette palette = m_timeLabel->palette();
    palette.setColor(m_timeLabel->foregroundRole(), Qt::white);
    m_timeLabel->setPalette(palette);
    m_openListButton->setIconSize(QSize(24, 24));
    m_openListButton->setColor(Qt::white);
    m_openListButton->setFixedWidth(42);
    QObject::connect(m_openListButton,&QtMaterialIconButton::clicked,this,[=]{m_drawer->openDrawer();});
    m_appBar->appBarLayout()->addWidget(m_openListButton);
    m_appBar->appBarLayout()->addWidget(m_timeLabel);
    m_appBar->appBarLayout()->addStretch(1);
    m_appBar->setGeometry(0,0,1280,50);
    m_appBar->setBackgroundColor(QColor(51,52,57));


    // 左侧列表栏
    m_drawer->setClickOutsideToClose(true);
    m_drawer->setOverlayMode(true);
    QVBoxLayout *drawerLayout = new QVBoxLayout;
    m_drawer->setDrawerLayout(drawerLayout);
    MyListWidget* listWidget = new MyListWidget;
    listWidget->setSize(230, 700);
    listWidget->addItem(tr("B L E"));
    listWidget->addItem(tr("U D P"));
    listWidget->addItem(tr("T C P"));
    listWidget->addItem(tr("H T T P"));
    listWidget->addItem(tr("C A N"));
    listWidget->addItem(tr("Service"));
    listWidget->addItem(tr("串 口"));
    listWidget->addItem(tr("音 乐"));
    listWidget->addItem(tr("设 置"));
    drawerLayout->addWidget(listWidget);
//    QVector<QString> labels = {"1（维护）", "2（维护）", "3（维护）", "4（维护）", "5（维护）", "6（维护）", "7（维护）"};
//    QVector<QString>::iterator it;
//    for (it = labels.begin(); it != labels.end(); ++it) {
//        QtMaterialFlatButton *m_testButton = new QtMaterialFlatButton(*it,this);
//        m_testButton->setMinimumHeight(50);
//        m_testButton->setFontSize(32);
//        m_testButton->setBackgroundColor("#222A59");
//        m_testButton->setForegroundColor("#0b80e0");
//        m_testButton->setBackgroundMode(Qt::OpaqueMode);
//        drawerLayout->addWidget(m_testButton);
//    }
    drawerLayout->addStretch(3);
    m_drawer->setContentsMargins(10, 0, 0, 0);
    m_closeListButton->setFontSize(32);
    m_closeListButton->setMinimumHeight(50);
    m_closeListButton->setBackgroundColor(QColor(209,26,65));
    m_closeListButton->setForegroundColor(QColor(179,186,206));
    m_drawer->setStyleSheet("background-color:rgb(51,52,57);color: rgb(179,186,206);");
    m_closeListButton->setHaloVisible(false);
    QObject::connect(m_closeListButton,&QtMaterialFlatButton::clicked,this,[=]{m_drawer->closeDrawer();});
    drawerLayout->addWidget(m_closeListButton);
    QObject::connect(listWidget,&MyListWidget::currentInterface,this,&MainInterface::switchMenuInterface);


    // 缩小和关闭
    m_narrowButton->setGeometry(this->width() - 80,7,35,35);
    m_narrowButton->setIconSize(QSize(35, 35));
    m_narrowButton->setColor(QColor(11,128,224));
    QObject::connect(m_narrowButton,&QtMaterialIconButton::released,this,[=]{emit narrowClicked();});

    m_closeButton->setGeometry(this->width() - 40,7,35,35);
    m_closeButton->setIconSize(QSize(30, 30));
    m_closeButton->setColor(QColor(228,7,51));
    QObject::connect(m_closeButton,&QtMaterialIconButton::released,this,[=]{m_dialog->show();m_dialog->showDialog();});

    m_scrollFont = new QLabel(this);
    m_scrollFont->setGeometry(200,0,this->width() - 400,50);
    m_scrollFont->setStyleSheet("QLabel{background-color:#00000000;color:#00ebdf;font:bold normal 20px '微软雅黑';}");
    m_scrollFont->setAlignment(Qt::AlignCenter);


    // 底栏
    m_tabs->addTab("首 页",QIcon(":/Image/BackgroundImage/Homepage.png"));
    m_tabs->addTab("房 间",QIcon(":/Image/BackgroundImage/Bedroom.png"));
    m_tabs->addTab("场 景",QIcon(":/Image/BackgroundImage/Scene.png"));
    m_tabs->addTab("安 防",QIcon(":/Image/BackgroundImage/Security.png"));
    m_tabs->addTab("监 控",QIcon(":/Image/BackgroundImage/Monitor.png"));
    m_tabs->addTab("更 多",QIcon(":/Image/BackgroundImage/More.png"));
    m_tabs->setGeometry(0,this->height() - 68,this->width(),68);
    m_tabs->setBackgroundColor(QColor(51,52,57));
    m_tabs->setHaloVisible(true);
    m_tabs->setStyleSheet("font: bold; font-size:20px; color: rgb(179,186,206); ");
    QObject::connect(m_tabs,&QtMaterialTabs::currentChanged,this,&MainInterface::switchInterface);

    // 音乐界面
    m_musicInterface = new MusicInterface(this,m_homePageInterface->getMusicPlayer());
    m_musicInterface->setGeometry(0,this->height() - 747,1680,747);

    // 弹窗
    QWidget *widget = new QWidget;
    QHBoxLayout *dialogWidgetLayout = new QHBoxLayout;
    QtMaterialRaisedButton *confirmCloseButton = new QtMaterialRaisedButton("Determine");
    QtMaterialRaisedButton *cancelCloseButton = new QtMaterialRaisedButton("Cancel");
    dialogWidgetLayout->addWidget(confirmCloseButton);
    dialogWidgetLayout->addWidget(cancelCloseButton);
    dialogWidgetLayout->setAlignment(confirmCloseButton, Qt::AlignBottom | Qt::AlignLeading);
    dialogWidgetLayout->setAlignment(cancelCloseButton, Qt::AlignBottom | Qt::AlignTrailing);
    widget->setLayout(dialogWidgetLayout);
    widget->setMinimumHeight(100);
    confirmCloseButton->setMinimumWidth(150);
    cancelCloseButton->setMinimumWidth(150);
    confirmCloseButton->setBackgroundColor(QColor(150,19,47));
    cancelCloseButton->setBackgroundColor(QColor(16,86,152));
    confirmCloseButton->setForegroundColor(QColor(179,186,206));
    cancelCloseButton->setForegroundColor(QColor(179,186,206));
    confirmCloseButton->setHaloVisible(false);
    cancelCloseButton->setHaloVisible(false);
    widget->setStyleSheet("background-color:rgb(51,52,57);");
    QVBoxLayout *dialogWidget = new QVBoxLayout;
    QLabel *exitPromptLabel = new QLabel("确 定 退 出 程 序 吗 ？");
    exitPromptLabel->setAlignment(Qt::AlignCenter);
    exitPromptLabel->setStyleSheet("QLabel{background-color:rgb(51,52,57);font: bold normal 32px 'Microsoft YaHei';color: rgb(179,186,206);}");
    dialogWidget->addWidget(widget);
    dialogWidget->addWidget(exitPromptLabel);
    dialogWidget->setDirection(QBoxLayout::BottomToTop);
    dialogWidget->setContentsMargins(0, 0, 0, 0);
    m_dialog->setWindowLayout(dialogWidget);
    QObject::connect(confirmCloseButton, &QtMaterialFlatButton::released, m_dialog, [=]{emit closeClicked();});
    QObject::connect(cancelCloseButton, &QtMaterialFlatButton::released, m_dialog, [=]{m_dialog->hide();m_dialog->hideDialog();});

    // 提示窗口
    m_snackbar->addInstantMessage(tr("首页界面待维护!"));
    m_snackbar->setClickToDismissMode(true);
    m_snackbar->setBackgroundOpacity(1);
    m_snackbar->setBackgroundColor(QColor(0,0,0));
    m_snackbar->setTextColor(QColor(209,26,65));
    m_snackbar->setBoxWidth(this->width());
    m_snackbar->setFontSize(25);

    // 系统时间
    m_systemTime = new QTimer();
    QObject::connect(m_systemTime, &QTimer::timeout, this, [=]{m_timeLabel->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd\nhh:mm:ss ddd"));m_homePageInterface->setWeather();});
    m_systemTime->start(1000);
}

MainInterface::~MainInterface()
{
    m_systemTime->stop();
    delete m_systemTime;
    m_systemTime = nullptr;
    delete ui;
}

void MainInterface::switchInterface(int num)
{
    if (m_currentInterface == num)
    {
        return;
    }
    switch (num) {
    case Interface::HomePage:
        switch (m_currentInterface) {
        case Interface::HomePage:
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            m_scenePageInterface->setVisible(false);
            break;
        case Interface::Security:
            m_securityInterface->setVisible(false);
            break;
        case Interface::Monitor:
            m_monitorInterface->setVisible(false);
            break;
        case Interface::More:
            m_moreInterface->setVisible(false);
            break;
        }
        m_homePageInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("首页界面待维护!"));
        m_currentInterface = Interface::HomePage;
        break;
    case Interface::Room:
        switch (m_currentInterface) {
        case Interface::HomePage:
            m_homePageInterface->setVisible(false);
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            m_scenePageInterface->setVisible(false);
            break;
        case Interface::Security:
            m_securityInterface->setVisible(false);
            break;
        case Interface::Monitor:
            m_monitorInterface->setVisible(false);
            break;
        case Interface::More:
            m_moreInterface->setVisible(false);
            break;
        }
        m_roomInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("房间界面待维护!"));
        m_currentInterface = Interface::Room;
        break;
    case Interface::ScenePage:
        switch (m_currentInterface) {
        case Interface::HomePage:
            m_homePageInterface->setVisible(false);
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            break;
        case Interface::Security:
            m_securityInterface->setVisible(false);
            break;
        case Interface::Monitor:
            m_monitorInterface->setVisible(false);
            break;
        case Interface::More:
            m_moreInterface->setVisible(false);
            break;
        }
        m_scenePageInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("场景界面待维护!"));
        m_currentInterface = Interface::ScenePage;
        break;
    case Interface::Security:
        switch (m_currentInterface) {
        case Interface::HomePage:
            m_homePageInterface->setVisible(false);
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            m_scenePageInterface->setVisible(false);
            break;
        case Interface::Security:
            break;
        case Interface::Monitor:
            m_monitorInterface->setVisible(false);
            break;
        case Interface::More:
            m_moreInterface->setVisible(false);
            break;
        }
        m_securityInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("安防界面待维护!"));
        m_currentInterface = Interface::Security;
        break;
    case Interface::Monitor:
        switch (m_currentInterface) {
        case Interface::HomePage:
            m_homePageInterface->setVisible(false);
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            m_scenePageInterface->setVisible(false);
            break;
        case Interface::Security:
            m_securityInterface->setVisible(false);
            break;
        case Interface::Monitor:
            break;
        case Interface::More:
            m_moreInterface->setVisible(false);
            break;
        }
        m_monitorInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("监控界面待维护!"));
        m_currentInterface = Interface::Monitor;
        break;
    case Interface::More:
        switch (m_currentInterface) {
        case Interface::HomePage:
            m_homePageInterface->setVisible(false);
            break;
        case Interface::Room:
            m_roomInterface->setVisible(false);
            break;
        case Interface::ScenePage:
            m_scenePageInterface->setVisible(false);
            break;
        case Interface::Security:
            m_securityInterface->setVisible(false);
            break;
        case Interface::Monitor:
            m_monitorInterface->setVisible(false);
            break;
        case Interface::More:
            break;
        }
        m_moreInterface->setVisible(true);
        m_snackbar->addInstantMessage(tr("更多界面待维护!"));
        m_currentInterface = Interface::More;
        break;
    default:
        break;
    }
}

void MainInterface::switchMenuInterface(int num)
{
    if (m_currentMenuInterface == num)
    {
        return;
    }
    switch (num) {
    case MenuInterface::BLE:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::BLE;
        break;
    case MenuInterface::UDP:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::UDP;
        break;
    case MenuInterface::TCP:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::TCP;
        break;
    case MenuInterface::HTTP:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::HTTP;
        break;
    case MenuInterface::CAN:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::CAN;
        break;
    case MenuInterface::Service:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::Service;
        break;
    case MenuInterface::SerialPort:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::SerialPort;
        break;
    case MenuInterface::Music:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::Music;
        break;
    case MenuInterface::SetUp:
        switch (m_currentMenuInterface) {
        case MenuInterface::BLE:
            break;
        case MenuInterface::UDP:
            break;
        case MenuInterface::TCP:
            break;
        case MenuInterface::HTTP:
            break;
        case MenuInterface::CAN:
            break;
        case MenuInterface::Service:
            break;
        case MenuInterface::SerialPort:
            break;
        case MenuInterface::Music:
            break;
        case MenuInterface::SetUp:
            break;
        default:
            break;
        }
        m_currentMenuInterface = MenuInterface::SetUp;
        break;
    default:
        break;
    }
}
