#ifndef MAININTERFACE_H
#define MAININTERFACE_H

#include <QWidget>
#include <QLabel>
#include <QTimer>
#include <QDateTime>
#include <QDebug>

#include <qtmaterialappbar.h>
#include <qtmaterialraisedbutton.h>
#include <qtmaterialiconbutton.h>
#include <qtmaterialflatbutton.h>
#include <lib/qtmaterialtheme.h>
#include <qtmaterialdrawer.h>
#include <qtmaterialtabs.h>
#include <qtmaterialdialog.h>
#include <qtmaterialsnackbar.h>

#include "animationlist/mylistwidget.h"

#include "HomePageInterface.h"
#include "RoomInterface.h"
#include "ScenePageInterface.h"
#include "SecurityInterface.h"
#include "MonitorInterface.h"
#include "MoreInterface.h"

#include "SubInterface/MusicInterface.h"

typedef enum {
    HomePage,
    Room,
    ScenePage,
    Security,
    Monitor,
    More
}Interface;

typedef enum {
    NoSub,
    MusicInformation,
    MusicList
}SubInterface;

typedef enum {
    BLE,
    UDP,
    TCP,
    HTTP,
    CAN,
    Service,
    SerialPort,
    Music,
    SetUp
}MenuInterface;

namespace Ui {
class MainInterface;
}

class MainInterface : public QWidget
{
    Q_OBJECT

public:
    explicit MainInterface(QWidget *parent = nullptr);
    ~MainInterface();
signals:
    void narrowClicked();
    void closeClicked();

private Q_SLOTS:
    void switchInterface(int num);
    void switchMenuInterface(int num);

private:
    Ui::MainInterface *ui;

    HomePageInterface *m_homePageInterface;
    RoomInterface *m_roomInterface;
    ScenePageInterface *m_scenePageInterface;
    SecurityInterface *m_securityInterface;
    MonitorInterface *m_monitorInterface;
    MoreInterface *m_moreInterface;

    MusicInterface *m_musicInterface;

    QtMaterialAppBar *const m_appBar;
    QtMaterialDrawer *const m_drawer;
    QtMaterialIconButton *const m_openListButton;
    QtMaterialRaisedButton *const m_closeListButton;
//    QtMaterialFlatButton *const m_testButton;
    QtMaterialIconButton *const m_narrowButton;
    QtMaterialIconButton *const m_closeButton;
    QtMaterialTabs *const m_tabs;
    QtMaterialDialog *const m_dialog;
    QtMaterialSnackbar *const m_snackbar;

    QLabel *m_scrollFont;

    Interface m_currentInterface = Interface::HomePage;
    SubInterface m_subInterface = SubInterface::NoSub;
    MenuInterface m_currentMenuInterface = MenuInterface::BLE;

    QLabel *m_timeLabel;
    QTimer *m_systemTime;
};

#endif // MAININTERFACE_H
