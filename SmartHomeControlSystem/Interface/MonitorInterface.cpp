#include "MonitorInterface.h"
#include "ui_MonitorInterface.h"

MonitorInterface::MonitorInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MonitorInterface)
{
    ui->setupUi(this);
    this->setVisible(false);
    m_joyStick = new JoyStick(this);
    m_joyStick->setGeometry(20,420,250,250);
    QObject::connect(m_joyStick,&JoyStick::sendCoordinates,this,[=](const char * data){qDebug() << data;});
}

MonitorInterface::~MonitorInterface()
{
    delete ui;
}
