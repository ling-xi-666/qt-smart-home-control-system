#ifndef MONITORINTERFACE_H
#define MONITORINTERFACE_H

#include <QWidget>

#include "joystick/joystick.h"

namespace Ui {
class MonitorInterface;
}

class MonitorInterface : public QWidget
{
    Q_OBJECT

public:
    explicit MonitorInterface(QWidget *parent = nullptr);
    ~MonitorInterface();

private:
    Ui::MonitorInterface *ui;

    JoyStick *m_joyStick;
};

#endif // MONITORINTERFACE_H
