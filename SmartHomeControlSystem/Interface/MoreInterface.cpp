#include "MoreInterface.h"
#include "ui_MoreInterface.h"

MoreInterface::MoreInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MoreInterface)
{
    ui->setupUi(this);
    this->setVisible(false);
}

MoreInterface::~MoreInterface()
{
    delete ui;
}
