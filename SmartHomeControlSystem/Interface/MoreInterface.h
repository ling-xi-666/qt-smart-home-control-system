#ifndef MOREINTERFACE_H
#define MOREINTERFACE_H

#include <QWidget>

namespace Ui {
class MoreInterface;
}

class MoreInterface : public QWidget
{
    Q_OBJECT

public:
    explicit MoreInterface(QWidget *parent = nullptr);
    ~MoreInterface();

private:
    Ui::MoreInterface *ui;
};

#endif // MOREINTERFACE_H
