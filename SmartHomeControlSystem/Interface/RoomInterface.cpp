#include "RoomInterface.h"
#include "ui_RoomInterface.h"

RoomInterface::RoomInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RoomInterface)
{
    ui->setupUi(this);
    this->setVisible(false);
}

RoomInterface::~RoomInterface()
{
    delete ui;
}
