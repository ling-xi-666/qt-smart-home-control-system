#ifndef ROOMINTERFACE_H
#define ROOMINTERFACE_H

#include <QWidget>

namespace Ui {
class RoomInterface;
}

class RoomInterface : public QWidget
{
    Q_OBJECT

public:
    explicit RoomInterface(QWidget *parent = nullptr);
    ~RoomInterface();

private:
    Ui::RoomInterface *ui;
};

#endif // ROOMINTERFACE_H
