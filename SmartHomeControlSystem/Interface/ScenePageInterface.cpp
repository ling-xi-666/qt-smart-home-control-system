#include "ScenePageInterface.h"
#include "ui_ScenePageInterface.h"

ScenePageInterface::ScenePageInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScenePageInterface)
{
    ui->setupUi(this);
    this->setVisible(false);
}

ScenePageInterface::~ScenePageInterface()
{
    delete ui;
}
