#ifndef SCENEPAGEINTERFACE_H
#define SCENEPAGEINTERFACE_H

#include <QWidget>

namespace Ui {
class ScenePageInterface;
}

class ScenePageInterface : public QWidget
{
    Q_OBJECT

public:
    explicit ScenePageInterface(QWidget *parent = nullptr);
    ~ScenePageInterface();

private:
    Ui::ScenePageInterface *ui;
};

#endif // SCENEPAGEINTERFACE_H
