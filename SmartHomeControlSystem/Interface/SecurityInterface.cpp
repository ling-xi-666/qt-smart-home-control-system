#include "SecurityInterface.h"
#include "ui_SecurityInterface.h"

SecurityInterface::SecurityInterface(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SecurityInterface)
{
    ui->setupUi(this);
    this->setVisible(false);
}

SecurityInterface::~SecurityInterface()
{
    delete ui;
}
