#ifndef SECURITYINTERFACE_H
#define SECURITYINTERFACE_H

#include <QWidget>

namespace Ui {
class SecurityInterface;
}

class SecurityInterface : public QWidget
{
    Q_OBJECT

public:
    explicit SecurityInterface(QWidget *parent = nullptr);
    ~SecurityInterface();

private:
    Ui::SecurityInterface *ui;
};

#endif // SECURITYINTERFACE_H
