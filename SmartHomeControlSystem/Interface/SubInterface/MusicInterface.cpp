#include "MusicInterface.h"
#include "ui_MusicInterface.h"

MusicInterface::MusicInterface(QWidget *parent,MusicPlayer *m_musicPlayer) :
    QWidget(parent),
    ui(new Ui::MusicInterface)
{
    QObject::connect(m_musicPlayer,&MusicPlayer::queryMusicInformation,this,&MusicInterface::updateList);
    QObject::connect(m_musicPlayer,&MusicPlayer::currentMusicInformation,this,[=](MUSIC_INFO musicINFO){this->m_songTitleButton->setText(musicINFO.Name);this->m_singerButton->setText(musicINFO.SingerName);this->m_albumNameButton->setText(musicINFO.AlbumName);m_durationButton->setText(timeConversion(musicINFO.Duration));this->m_currentTotalTimeButton->setText(timeConversion(musicINFO.Duration));});
    QObject::connect(m_musicPlayer,&MusicPlayer::currentLyrics,this,[=](QString lyrics){this->m_lyricButton->setText(lyrics);});
    QObject::connect(m_musicPlayer,&MusicPlayer::updateMusicImage,this,[=](){this->m_musicImage->setImage(QImage(QDir::currentPath() + "/music.jpg"));});
    QObject::connect(m_musicPlayer,&MusicPlayer::currentMusicPosition,this,[=](qint64 position,qint64 duration){this->m_musicslider->setRange(0,duration);this->m_musicslider->setValue(position);this->m_currentTimeButton->setText(timeConversion(position));});
    QObject::connect(m_musicPlayer,&MusicPlayer::musicStateChanged,this,[=](QMediaPlayer::State newState){
        switch (newState) {
        case QMediaPlayer::StoppedState:
            this->m_isPlayMusic = false;
            this->m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Play.png"));
            break;
        case QMediaPlayer::PlayingState:
            this->m_isPlayMusic = true;
            this->m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Suspend.png"));
            break;
        case QMediaPlayer::PausedState:
            this->m_isPlayMusic = false;
            this->m_suspendButton->setIcon(QIcon(":/Image/HomePageInterface/Play.png"));
            break;
        }});
    ui->setupUi(this);
    ui->m_musicListWidget->setVisible(false);
    this->setVisible(false);

    this->m_bluetoothButton = new QtMaterialIconButton(QIcon(":/Image/MusicInterface/Bluetooth.png"),ui->m_musicInformationWidget);
    this->m_musicListButton = new QtMaterialIconButton(QIcon(":/Image/MusicInterface/MusicList.png"),ui->m_musicInformationWidget);
    QObject::connect(this->m_musicListButton,&QtMaterialIconButton::released,this,[=]{ui->m_musicInformationWidget->setVisible(false);ui->m_musicListWidget->setVisible(true);});
    this->m_bluetoothButton->setGeometry(1080,20,48,48);
    this->m_musicListButton->setGeometry(1150,20,50,50);
    this->m_bluetoothButton->setIconSize(QSize(48,48));
    this->m_musicListButton->setIconSize(QSize(50,50));
    this->m_bluetoothButton->setColor(QColor(191,198,220));
    this->m_musicListButton->setColor(QColor(191,198,220));

    this->m_musicImage = new QtMaterialAvatar(QImage(":/Image/HomePageInterface/NoData.png"),ui->m_musicInformationWidget);
    this->m_musicImage->setGeometry(150,100,400,400);
    this->m_musicImage->setSize(400);

    this->m_songTitleButton = new QtMaterialFlatButton(tr("音乐名称"),ui->m_musicInformationWidget);
    this->m_songTitleButton->setGeometry(600,95,500,70);
    this->m_songTitleButton->setForegroundColor(QColor(191,198,220));
    this->m_songTitleButton->setStyleSheet("QPushButton{background-color:#00000000;font:bold normal 40px '微软雅黑';color:#FFFFFF;}");
    this->m_songTitleButton->setHaloVisible(false);

    this->m_singerButton = new QtMaterialFlatButton(tr("歌手名称"),ui->m_musicInformationWidget);
    this->m_singerButton->setGeometry(600,175,500,70);
    this->m_singerButton->setForegroundColor(QColor(191,198,220));
    this->m_singerButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑';}");
    this->m_singerButton->setHaloVisible(false);

    this->m_albumNameButton = new QtMaterialFlatButton(tr("专辑名称"),ui->m_musicInformationWidget);
    this->m_albumNameButton->setGeometry(600,255,500,70);
    this->m_albumNameButton->setForegroundColor(QColor(191,198,220));
    this->m_albumNameButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑';}");
    this->m_albumNameButton->setHaloVisible(false);

    this->m_durationButton = new QtMaterialFlatButton(tr("持续时间"),ui->m_musicInformationWidget);
    this->m_durationButton->setGeometry(600,335,500,70);
    this->m_durationButton->setForegroundColor(QColor(191,198,220));
    this->m_durationButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑'}");
    this->m_durationButton->setHaloVisible(false);


    this->m_playbackModeButton = new QtMaterialIconButton(QIcon(":/Image/MusicInterface/SequentialPlayback.png"),ui->m_musicInformationWidget);
    QObject::connect(this->m_playbackModeButton,&QtMaterialIconButton::released,this,[=]{
        switch (m_MusicPlaybackMode) {
        case MusicPlaybackMode::SingleHeadCycle:
            this->m_MusicPlaybackMode = MusicPlaybackMode::SequentialPlayback;
            this->m_playbackModeButton->setIcon(QIcon(":/Image/MusicInterface/SequentialPlayback.png"));
            m_musicPlayer->setPlaybackMode(QMediaPlaylist::PlaybackMode::Loop);
            break;
        case MusicPlaybackMode::SequentialPlayback:
            this->m_MusicPlaybackMode = MusicPlaybackMode::RandomPlay;
            this->m_playbackModeButton->setIcon(QIcon(":/Image/MusicInterface/RandomPlay.png"));
            m_musicPlayer->setPlaybackMode(QMediaPlaylist::PlaybackMode::Random);
            break;
        case MusicPlaybackMode::RandomPlay:
            this->m_MusicPlaybackMode = MusicPlaybackMode::SingleHeadCycle;
            this->m_playbackModeButton->setIcon(QIcon(":/Image/MusicInterface/SingleHeadCycle.png"));
            m_musicPlayer->setPlaybackMode(QMediaPlaylist::PlaybackMode::CurrentItemInLoop);
            break;
        default:
            break;
        }
    });
    this->m_playbackModeButton->setGeometry(650,410,50,50);
    this->m_playbackModeButton->setIconSize(QSize(50,50));
    this->m_playbackModeButton->setColor(QColor(191,198,220));

    this->m_lyricButton = new QtMaterialFlatButton(ui->m_musicInformationWidget);
    this->m_lyricButton->setGeometry(200,500,this->width() - 400,70);
    this->m_lyricButton->setForegroundColor(QColor(191,198,220));
    this->m_lyricButton->setStyleSheet("QPushButton{background-color:#00000000;font:bold normal 30px '微软雅黑';color:#FFFFFF;}");
    this->m_lyricButton->setHaloVisible(false);

    this->m_musicslider = new QtMaterialSlider(ui->m_musicInformationWidget);
    QObject::connect(this->m_musicslider,&QtMaterialSlider::sliderMoved, this,[=](qint64 value){m_musicPlayer->setCurrentMusicPosition(value);});
    this->m_musicslider->setGeometry(200,560,this->width() - 400,50);
    this->m_musicslider->setMaximumHeight(50);
    this->m_musicslider->setMinimumHeight(50);
    this->m_musicslider->setThumbColor(QColor(191,198,220));
    QColor color(QColor(63,66,76));
    color.setAlpha(100);
    this->m_musicslider->setTrackColor(color);
    this->m_musicslider->setDisabledColor(QColor(63,66,76));
    this->m_musicslider->setDisabled(false);

    m_currentTimeButton = new QtMaterialFlatButton(ui->m_musicInformationWidget);
    m_currentTotalTimeButton = new QtMaterialFlatButton(ui->m_musicInformationWidget);
    this->m_currentTimeButton->setGeometry(200,600,100,20);
    this->m_currentTotalTimeButton->setGeometry(this->width() - 300,600,100,20);
    this->m_currentTimeButton->setForegroundColor(QColor(191,198,220));
    this->m_currentTotalTimeButton->setForegroundColor(QColor(191,198,220));
    this->m_currentTimeButton->setStyleSheet("QPushButton{background-color:#00000000;font:bold normal 20px '微软雅黑';color:#FFFFFF;}");
    this->m_currentTotalTimeButton->setStyleSheet("QPushButton{background-color:#00000000;font:bold normal 20px '微软雅黑';color:#FFFFFF;}");
    this->m_currentTimeButton->setHaloVisible(false);
    this->m_currentTotalTimeButton->setHaloVisible(false);

    this->m_previousSongButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/PreviousSong.png"),ui->m_musicInformationWidget);
    this->m_suspendButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/Play.png"),ui->m_musicInformationWidget);
    this->m_nextSongButton = new QtMaterialIconButton(QIcon(":/Image/HomePageInterface/NextSong.png"),ui->m_musicInformationWidget);
    QObject::connect(this->m_previousSongButton,&QtMaterialIconButton::released,this,[=]{m_musicPlayer->switchMusicState(SwitchMusicState::PreviousSong);});
    QObject::connect(this->m_suspendButton,&QtMaterialIconButton::released,this,[=](){this->m_isPlayMusic = !this->m_isPlayMusic;this->m_isPlayMusic ? m_musicPlayer->switchMusicState(SwitchMusicState::PlayCurrent) : m_musicPlayer->switchMusicState(SwitchMusicState::PausePlayback);});
    QObject::connect(this->m_nextSongButton,&QtMaterialIconButton::released,this,[=]{m_musicPlayer->switchMusicState(SwitchMusicState::NextSong);});
    this->m_previousSongButton->setGeometry(415,620,100,100);
    this->m_suspendButton->setGeometry(590,620,100,100);
    this->m_nextSongButton->setGeometry(768,620,100,100);
    this->m_previousSongButton->setIconSize(QSize(100,100));
    this->m_suspendButton->setIconSize(QSize(100,100));
    this->m_nextSongButton->setIconSize(QSize(100,100));
    this->m_previousSongButton->setColor(QColor(191,198,220));
    this->m_suspendButton->setColor(QColor(191,198,220));
    this->m_nextSongButton->setColor(QColor(191,198,220));
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    QFont font;
    font.setFamily("微软雅黑");
    font.setPointSize(20);
    font.setBold(true);

    this->m_netEaseCloud = new QtMaterialRadioButton(this->ui->m_musicListWidget);
    QObject::connect(this->m_netEaseCloud,&QtMaterialRadioButton::toggled,this,[=]{m_musicPlayer->setMusicQueryAddress(MusicQueryAddress::NetEaseCloudPlayer);});
    this->m_local = new QtMaterialRadioButton(this->ui->m_musicListWidget);
    QObject::connect(this->m_local,&QtMaterialRadioButton::toggled,this,[=]{m_musicPlayer->setMusicQueryAddress(MusicQueryAddress::LocalPlayer);});
    this->m_recentlyPlayed = new QtMaterialRadioButton(this->ui->m_musicListWidget);
    QObject::connect(this->m_recentlyPlayed,&QtMaterialRadioButton::toggled,this,[=]{m_musicPlayer->setMusicQueryAddress(MusicQueryAddress::RecentlyPlayed);});
    this->m_netEaseCloud->setGeometry(20,30,130,40);
    this->m_local->setGeometry(150,30,200,40);
    this->m_recentlyPlayed->setGeometry(320,30,200,40);
    this->m_netEaseCloud->setFont(font);
    this->m_local->setFont(font);
    this->m_recentlyPlayed->setFont(font);
    this->m_netEaseCloud->setTextColor(QColor(191,198,220));
    this->m_local->setTextColor(QColor(191,198,220));
    this->m_recentlyPlayed->setTextColor(QColor(191,198,220));
    this->m_netEaseCloud->setUncheckedColor(QColor(191,198,220));
    this->m_local->setUncheckedColor(QColor(191,198,220));
    this->m_recentlyPlayed->setUncheckedColor(QColor(191,198,220));
    this->m_netEaseCloud->setText(tr("网易云"));
    this->m_local->setText(tr("本地音乐"));
    this->m_recentlyPlayed->setText(tr("最近播放"));

    this->m_searchMusicText = new QtMaterialTextField(this->ui->m_musicListWidget);
    QObject::connect(this->m_searchMusicText, &QtMaterialTextField::editingFinished,this,[=]{m_musicPlayer->searchMusic(m_searchMusicText->text());});
    this->m_searchMusicButton = new QtMaterialIconButton(QIcon(":/Image/MusicInterface/Search.png"),this->ui->m_musicListWidget);
    QObject::connect(this->m_searchMusicButton,&QtMaterialIconButton::released,this,[=]{m_musicPlayer->searchMusic(m_searchMusicText->text());});
    this->m_searchMusicText->setGeometry(490,20,300,50);
    this->m_searchMusicButton->setGeometry(740,20,50,50);
    this->m_searchMusicButton->setIconSize(QSize(50,50));
    this->m_searchMusicText->setFont(font);
    this->m_searchMusicText->setInputLineColor(QColor(70,74,85));
    this->m_searchMusicText->setInkColor(QColor(191,198,220));
    this->m_searchMusicText->setLabelColor(QColor(191,198,220));
    this->m_searchMusicText->setTextColor(QColor(191,198,220));
    this->m_searchMusicButton->setColor(QColor(191,198,220));
    this->m_searchMusicText->setPlaceholderText(tr("请输入搜索歌名"));
    this->m_searchMusicText->setShowLabel(false);
    this->m_selectMusicFolder = new QtMaterialRaisedButton(tr("选择本地音乐文件夹"), this->ui->m_musicListWidget);
    QObject::connect(this->m_selectMusicFolder,&QtMaterialFlatButton::released,this,[=]{m_musicPlayer->readMusicfile(QFileDialog::getExistingDirectory());});
    this->m_selectMusicFolder->setGeometry(800,20,400,50);
    this->m_selectMusicFolder->setFontSize(30);
    this->m_selectMusicFolder->setForegroundColor(QColor(191,198,220));
    this->m_selectMusicFolder->setBackgroundColor(QColor(70,74,85));
    this->ui->m_listWidget->setFrameShape(QListWidget::NoFrame);
    QObject::connect(this->ui->m_listWidget,&QListWidget::itemDoubleClicked,this,[=](QListWidgetItem *item){m_musicPlayer->addMusic(item->text().toInt());});


    this->m_songTitleTextButton = new QtMaterialFlatButton(tr("歌名"),ui->m_musicListWidget);
    this->m_songTitleTextButton->setGeometry(0,86,800,70);
    this->m_songTitleTextButton->setForegroundColor(QColor(191,198,220));
    this->m_songTitleTextButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑';}");
    this->m_songTitleTextButton->setHaloVisible(false);

    this->m_singerTextButton = new QtMaterialFlatButton(tr("歌手"),ui->m_musicListWidget);
    this->m_singerTextButton->setGeometry(800,86,240,70);
    this->m_singerTextButton->setForegroundColor(QColor(191,198,220));
    this->m_singerTextButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑';}");
    this->m_singerTextButton->setHaloVisible(false);

    this->m_durationTextButton = new QtMaterialFlatButton(tr("时间"),ui->m_musicListWidget);
    this->m_durationTextButton->setGeometry(1040,86,240,70);
    this->m_durationTextButton->setForegroundColor(QColor(191,198,220));
    this->m_durationTextButton->setStyleSheet("QPushButton{font:bold normal 30px '微软雅黑';}");
    this->m_durationTextButton->setHaloVisible(false);
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    this->m_returnButton = new QtMaterialIconButton(QIcon(":/Image/MusicInterface/Return.png"),this);
    QObject::connect(this->m_returnButton,&QtMaterialIconButton::released,this,[=]{
        if(ui->m_musicListWidget->isVisible())
        {
            ui->m_musicListWidget->setVisible(false);
            ui->m_musicInformationWidget->setVisible(true);
        }
        else
        {
            this->setVisible(false);
        }
    });
    this->m_returnButton->setGeometry(1220,20,50,50);
    this->m_returnButton->setIconSize(QSize(50,50));
    this->m_returnButton->setColor(QColor(191,198,220));
}

MusicInterface::~MusicInterface()
{
    delete ui;
}

void MusicInterface::updateList(QList<MUSIC_INFO> musicListINFO)
{
    ui->m_listWidget->clear();

    for (int var = 0; var < musicListINFO.size(); ++var) {
        QListWidgetItem *listWidgetItem = new QListWidgetItem(ui->m_listWidget);
        listWidgetItem->setSizeHint(QSize(744,57));
        listWidgetItem->setText(QString::number(var));

        QWidget *listWidget = new QWidget(this);
        listWidget->setStyleSheet("QWidget{border-radius: 0px 0px 0px 0px;border-top: 1px solid #FFFFFF;}");
        QLabel *songTitleText = new QLabel(musicListINFO.at(var).Name,listWidget);
        QLabel *singerText = new QLabel(musicListINFO.at(var).SingerName,listWidget);
        QLabel *durationText = new QLabel(timeConversion(musicListINFO.at(var).Duration),listWidget);

        songTitleText->setGeometry(0,0,800,56);
        singerText->setGeometry(800,0,240,56);
        durationText->setGeometry(1040,0,240,56);

        songTitleText->setStyleSheet("QLabel{font-size: 25px;font-family: PingFangSC-Regular, PingFang SC;font-weight: 400;color: rgb(191,198,220);line-height: 22px;border-top: 1px solid rgb(191,198,220);text-align: center;}");
        singerText->setStyleSheet("QLabel{font-size: 25px;font-family: PingFangSC-Regular, PingFang SC;font-weight: 400;color: rgb(191,198,220);line-height: 22px;border-top: 1px solid rgb(191,198,220);text-align: center;}");
        durationText->setStyleSheet("QLabel{font-size: 25px;font-family: PingFangSC-Regular, PingFang SC;font-weight: 400;color: rgb(191,198,220);line-height: 22px;border-top: 1px solid rgb(191,198,220);text-align: center;}");

        singerText->setAlignment(Qt::AlignCenter);
        durationText->setAlignment(Qt::AlignCenter);
        singerText->setWordWrap(true);
        ui->m_listWidget->addItem(listWidgetItem);
        ui->m_listWidget->setItemWidget(listWidgetItem,listWidget);
    }
}

QString MusicInterface::timeConversion(int time)
{
    time /= 1000;
    QString h = time/3600 >= 10 ? QString::number(time/3600) : "0" + QString::number(time/3600);
    QString m = (time-h.toInt()*3600)/60 >= 10 ? QString::number((time-h.toInt()*3600)/60) : "0" + QString::number((time-h.toInt()*3600)/60);
    QString s = time-h.toInt()*3600-m.toInt()*60 >= 10 ? QString::number(time-h.toInt()*3600-m.toInt()*60) : "0" + QString::number(time-h.toInt()*3600-m.toInt()*60);
    return h.toInt() > 0 ? QString(h + ":" + m + ":" + s) : QString(m + ":" + s);
}
