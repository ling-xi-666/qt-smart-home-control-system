#ifndef MUSICINTERFACE_H
#define MUSICINTERFACE_H

#include <QWidget>
#include <MusicPlayer.h>

#include <qtmaterialavatar.h>
#include <qtmaterialflatbutton.h>
#include <qtmaterialiconbutton.h>
#include <qtmaterialslider.h>
#include <qtmaterialtextfield.h>
#include <qtmaterialradiobutton.h>
#include <qtmaterialraisedbutton.h>

namespace Ui {
class MusicInterface;
}

typedef enum{
    SingleHeadCycle,
    SequentialPlayback,
    RandomPlay
}MusicPlaybackMode;

class MusicInterface : public QWidget
{
    Q_OBJECT

public:
    explicit MusicInterface(QWidget *parent = nullptr,MusicPlayer *music = nullptr);
    ~MusicInterface();

private slots:

private:
    QString timeConversion(int time);
    void updateList(QList<MUSIC_INFO> musicListINFO);
private:
    Ui::MusicInterface *ui;
private:
    QtMaterialIconButton *m_bluetoothButton;
    QtMaterialIconButton *m_musicListButton;

    QtMaterialAvatar *m_musicImage;
    QtMaterialFlatButton *m_songTitleButton;
    QtMaterialFlatButton *m_singerButton;
    QtMaterialFlatButton *m_albumNameButton;
    QtMaterialFlatButton *m_durationButton;

    QtMaterialIconButton *m_playbackModeButton;
    QtMaterialSlider *m_musicslider;
    QtMaterialFlatButton *m_currentTimeButton;
    QtMaterialFlatButton *m_currentTotalTimeButton;
    QtMaterialFlatButton *m_lyricButton;
    QtMaterialIconButton *m_previousSongButton;
    QtMaterialIconButton *m_suspendButton;
    QtMaterialIconButton *m_nextSongButton;
    //--------------------------------------------------------------------------------------
    QtMaterialRadioButton *m_netEaseCloud;
    QtMaterialRadioButton *m_local;
    QtMaterialRadioButton *m_recentlyPlayed;
    QtMaterialTextField *m_searchMusicText;
    QtMaterialIconButton *m_searchMusicButton;
    QtMaterialRaisedButton *m_selectMusicFolder;

    QtMaterialFlatButton *m_songTitleTextButton;
    QtMaterialFlatButton *m_singerTextButton;
    QtMaterialFlatButton *m_albumNameTextButton;
    QtMaterialFlatButton *m_durationTextButton;
    //--------------------------------------------------------------------------------------
    QtMaterialIconButton *m_returnButton;
private:
    MusicPlaybackMode m_MusicPlaybackMode = MusicPlaybackMode::SequentialPlayback;
    bool m_isPlayMusic = false;
};

#endif // MUSICINTERFACE_H
