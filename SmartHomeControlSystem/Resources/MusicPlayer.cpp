#include "MusicPlayer.h"

MusicPlayer::MusicPlayer(QObject *parent) :
    QObject(parent)
{
    m_musicThread = new QThread(this);
    m_player = new QMediaPlayer();
    QObject::connect(this,&MusicPlayer::playCurrentMusic,m_player,&QMediaPlayer::play);
    QObject::connect(this,&MusicPlayer::pauseCurrentMusic,m_player,&QMediaPlayer::pause);
    QObject::connect(this,&MusicPlayer::setMusicPosition,m_player,&QMediaPlayer::setPosition);
    QObject::connect(this,&MusicPlayer::setMusicPlaylist,m_player,&QMediaPlayer::setPlaylist);
    QObject::connect(this,&MusicPlayer::setMusicMedia,m_player,&QMediaPlayer::setMedia);
    m_musicList = new QMediaPlaylist(this);

    QObject::connect(m_musicList,&QMediaPlaylist::currentIndexChanged,this,[=](int current){if (current >= 0){this->loadMusicLyrics(m_currentMusicListINFO.at(current));this->loadMusicImage(m_currentMusicListINFO.at(current));emit currentMusicInformation(m_currentMusicListINFO.at(current));emit currentLyrics(m_currentMusicListINFO.at(current).Name);}});
    QObject::connect(m_player,&QMediaPlayer::stateChanged,this,[=](QMediaPlayer::State newState){
        switch(newState){
        case QMediaPlayer::State::StoppedState:
            m_lyricsTimer->stop();
            break;
        case QMediaPlayer::State::PlayingState:
            m_lyricsTimer->start(900);
            break;
        case QMediaPlayer::State::PausedState:
            m_lyricsTimer->stop();
            break;
        }
        emit musicStateChanged(newState);
    });
    QObject::connect(m_player,&QMediaPlayer::durationChanged,this,[=](qint64 duration){
        m_currentMusicDuration = duration;
    });
    QObject::connect(m_player,&QMediaPlayer::positionChanged,this,[=](qint64 position){
        if(m_lyricsTimer->isActive())
            emit currentMusicPosition(position,m_currentMusicDuration);
    });

    m_lyricsTimer = new QTimer();
    QObject::connect(m_lyricsTimer,&QTimer::timeout,this,[=]{
        QTime max_time;
        max_time.setHMS(0, m_player->position()/60000,qRound((m_player->position()%60000)/1000.0));
        for (QPair<QTime, QString> data : m_lyrics)
        {
            if (data.first == max_time)
            {
                emit currentLyrics(data.second);
            }
        }});
    m_delayPlayMusicTimer = new QTimer();
    QObject::connect(m_delayPlayMusicTimer,&QTimer::timeout,this,[=]{
        m_delayPlayMusicTimer->stop();
        QMetaObject::invokeMethod(this, "playCurrentMusic", Qt::QueuedConnection);
    });


    m_musicInformationManager = new QNetworkAccessManager(this);
    m_musicLyricsManager = new QNetworkAccessManager(this);
    m_musicIMGManager = new QNetworkAccessManager(this);

    QObject::connect(m_musicInformationManager,&QNetworkAccessManager::finished,this,&MusicPlayer::replyMusicInformationDone);
    QObject::connect(m_musicLyricsManager,&QNetworkAccessManager::finished,this,&MusicPlayer::replyMusicLyricsDone);
    QObject::connect(m_musicIMGManager,&QNetworkAccessManager::finished,this,&MusicPlayer::replyMusicImageFinished);
    m_musicList->setPlaybackMode(QMediaPlaylist::Sequential);

    m_player->moveToThread(m_musicThread);
    m_musicThread->start(QThread::Priority::LowestPriority);
    QObject::connect(m_musicThread, &QThread::finished, m_player, &QMediaPlayer::deleteLater);
}

MusicPlayer::~MusicPlayer()
{
    if(m_musicThread)
    {
        m_musicThread->quit();
        m_musicThread->wait();
        delete m_musicThread;
        m_musicThread = nullptr;
    }

    m_lyricsTimer->stop();
    m_delayPlayMusicTimer->stop();
    m_player->stop();
    delete m_player;
    m_player = nullptr;
    m_musicList->clear();
    delete m_musicList;
    m_musicList = nullptr;
    delete m_lyricsTimer;
    m_lyricsTimer = nullptr;
    delete m_delayPlayMusicTimer;
    m_delayPlayMusicTimer = nullptr;
}

void MusicPlayer::setMusicQueryAddress(MusicQueryAddress mode)
{
    m_musicMusicQueryAddress = mode;
    switch (m_musicMusicQueryAddress)
    {
    case MusicQueryAddress::Nothing:
        break;
    case MusicQueryAddress::NetEaseCloudPlayer:
        this->searchMusic(m_currentNetEaseCloudName);
        break;
    case MusicQueryAddress::LocalPlayer:
        this->readMusicfile(m_localMusicPath);
        break;
    case MusicQueryAddress::RecentlyPlayed:
        emit queryMusicInformation(m_currentMusicListINFO);
        break;
    default:
        break;
    }
}

void MusicPlayer::searchMusic(QString musicName)
{
    switch (m_musicMusicQueryAddress)
    {
    case MusicQueryAddress::Nothing:
        break;
    case MusicQueryAddress::NetEaseCloudPlayer:
    {
        m_currentNetEaseCloudName = musicName;
        QNetworkRequest request;
        request.setUrl(QString("http://iwxyi.com:3000/search?keywords=%0").arg(musicName));
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded; charset=UTF-8");
        request.setHeader(QNetworkRequest::UserAgentHeader, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36");

        m_musicInformationManager->get(request);
        break;
    }
    case MusicQueryAddress::LocalPlayer:
        break;
    case MusicQueryAddress::RecentlyPlayed:
        break;
    default:
        break;
    }
}

void MusicPlayer::addMusic(int index)
{
    switchMusicState(SwitchMusicState::PausePlayback);
    switch (m_musicMusicQueryAddress)
    {
    case MusicQueryAddress::Nothing:
        break;
    case MusicQueryAddress::NetEaseCloudPlayer:
        m_musicList->insertMedia(0,QUrl(QString("http://music.163.com/song/media/outer/url?id=%0").arg(m_musicListINFO.at(index).MusicID)));
        break;
    case MusicQueryAddress::LocalPlayer:
        m_musicList->insertMedia(0,QUrl::fromLocalFile(m_localMusicPath + '/' + m_musicListINFO.at(index).Name));
        break;
    case MusicQueryAddress::RecentlyPlayed:
        break;
    default:
        break;
    }
    m_currentMusicListINFO.insert(0,m_musicListINFO.at(index));
    emit setMusicPlaylist(m_musicList);
    emit setMusicMedia(m_musicList);
    this->switchMusicState(SwitchMusicState::PlayCurrent);
}

void MusicPlayer::switchMusicState(SwitchMusicState state)
{
    switch (state) {
    case SwitchMusicState::PreviousSong:
        m_musicList->previous();
        break;
    case SwitchMusicState::PlayCurrent:
        QMetaObject::invokeMethod(this, "playCurrentMusic", Qt::QueuedConnection);
        break;
    case SwitchMusicState::PausePlayback:
        QMetaObject::invokeMethod(this, "pauseCurrentMusic", Qt::QueuedConnection);
        break;
    case SwitchMusicState::NextSong:
        m_musicList->next();
        break;
    default:
        break;
    }
}

void MusicPlayer::readMusicfile(QString musicPath)
{
    m_musicListINFO.clear();
    m_localMusicPath = musicPath;
    QDir dir(musicPath);
    QStringList nameFileters;
    nameFileters << "*.mp3" << "*.wav";
    QStringList files = dir.entryList(nameFileters,QDir::Files|QDir::Readable,QDir::Name);
    if(files.size() >= 1)
    {
        for (QString file: files)
        {
            MUSIC_INFO info;
            info.Name = file;
            m_musicListINFO.append(info);
        }
    }
    if(m_musicMusicQueryAddress == MusicQueryAddress::LocalPlayer)
        emit queryMusicInformation(m_musicListINFO);
}

void MusicPlayer::setCurrentMusicPosition(qint64 position)
{
    this->switchMusicState(SwitchMusicState::PausePlayback);
    m_delayPlayMusicTimer->stop();
    QMetaObject::invokeMethod(this, "setMusicPosition", Qt::QueuedConnection, Q_ARG(qint64, position));
    m_delayPlayMusicTimer->start(500);
}

void MusicPlayer::setPlaybackMode(QMediaPlaylist::PlaybackMode mode)
{
    m_musicList->setPlaybackMode(mode);
}

void MusicPlayer::replyMusicInformationDone(QNetworkReply *reply)
{
    m_musicListINFO.clear();
    QJsonParseError err;
    QJsonDocument json = QJsonDocument::fromJson(reply->readAll(),&err);
    QStringList keys = json.object().keys();
    if(keys.contains("result") && json.object()["result"].toObject().contains("songs"))
    {
        for(auto i : json.object()["result"].toObject()["songs"].toArray())
        {
            MUSIC_INFO info;
            info.MusicID = i.toObject()["id"].toInt();
            info.Duration = i.toObject()["duration"].toInt();
            info.Name = i.toObject()["name"].toString();
            info.MVID = i.toObject()["mvId"].toInt();
            foreach (QJsonValue v, i.toObject()["artists"].toArray()) {
                info.SingerName = v.toObject()["name"].toString();
                info.img1v1Url = v.toObject()["img1v1Url"].toString();
            }
            m_musicListINFO.append(info);
        }
    }
    if(m_musicMusicQueryAddress == MusicQueryAddress::NetEaseCloudPlayer)
        emit queryMusicInformation(m_musicListINFO);
}

void MusicPlayer::loadMusicLyrics(MUSIC_INFO musicINFO)
{
    QNetworkRequest request;
    request.setUrl(QUrl(QString("http://music.163.com/api/song/lyric?id=%0&lv=-1&kv=-1&tv=-1").arg(musicINFO.MusicID)));
    m_musicLyricsManager->get(request);
}

void MusicPlayer::replyMusicLyricsDone(QNetworkReply *reply)
{
    m_lyrics.clear();
    QString lyrics;
    QStringList lyricsList = QString(reply->readAll()).split("[");
    if (lyricsList.size() > 1)
    {
        lyricsList.removeAt(0);
        for (QString lyric: lyricsList)
        {
            lyrics.append(lyric);
        }
        lyricsList = lyrics.split("\"}");
        if (lyricsList.size() > 1)
        {
            lyricsList = QString(lyricsList[0]).split("\\n");
            for (QString lyric: lyricsList)
            {
                QStringList ly = lyric.split("]");
                if (ly.size() > 1)
                {
                    QStringList timeList = ly.at(0).split(".");
                    QPair<QTime, QString> data(QTime::fromString(timeList.at(0), "mm:ss"),ly.at(1));
                    m_lyrics.append(data);
                }
            }
        }
    }
}

void MusicPlayer::loadMusicImage(MUSIC_INFO musicINFO)
{
    QNetworkRequest request;
    request.setUrl(QUrl(musicINFO.img1v1Url.remove(4,1)));
    m_musicIMGManager->get(request);
}

void MusicPlayer::replyMusicImageFinished(QNetworkReply *reply)
{
    QByteArray bytes = reply->readAll();
    QFile file(QDir::currentPath() + "/music.jpg");
    if (file.open(QIODevice::ReadWrite))
        file.write(bytes);
    file.close();
    emit updateMusicImage();
}
