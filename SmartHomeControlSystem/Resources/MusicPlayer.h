#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <QTimer>
#include <QThread>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>
#include <QDir>
#include <QPair>

typedef struct
{
    int MusicID;        // 音乐ID
    int Duration;       // 持续时间
    QString Name;       // 歌名
    int MVID;           // MVID
    QString SingerName; // 歌手名
    QString AlbumName;  // 专辑名
    QString img1v1Url;  // IMGurl
} MUSIC_INFO;

typedef enum
{
    Nothing,
    NetEaseCloudPlayer,
    LocalPlayer,
    RecentlyPlayed
}MusicQueryAddress;

typedef enum
{
    PreviousSong,
    PlayCurrent,
    PausePlayback,
    NextSong
} SwitchMusicState;

class MusicPlayer : public QObject
{
    Q_OBJECT
public:
    explicit MusicPlayer(QObject *parent = nullptr);
    ~MusicPlayer();
signals:
    // 当前播放音乐的信息
    void currentMusicInformation(MUSIC_INFO musicINFO);
    // 当前音乐的歌词
    void currentLyrics(QString lyrics);
    // 更新音乐封面
    void updateMusicImage();

    // 音乐播放器状态
    void musicStateChanged(QMediaPlayer::State newState);
    // 当前音乐进度
    void currentMusicPosition(qint64 position,qint64 duration);
    // 查询音乐信息
    void queryMusicInformation(QList<MUSIC_INFO> musicListINFO);

    // 控制音乐线程
    void playCurrentMusic();
    void pauseCurrentMusic();
    void setMusicPlaylist(QMediaPlaylist *playlist);
    void setMusicMedia(const QMediaContent &media, QIODevice *stream = nullptr);
    void setMusicPosition(qint64 position);
public slots:
    // 设置音乐搜索通道
    void setMusicQueryAddress(MusicQueryAddress mode);
    // 搜索音乐
    void searchMusic(QString musicName);
    // 添加音乐
    void addMusic(int index);
    // 切换音乐状态
    void switchMusicState(SwitchMusicState switchMusic);
    // 读取本地音乐（本地）
    void readMusicfile(QString musicPath);
    // 设置当前音乐进度
    void setCurrentMusicPosition(qint64 position);
    // 设置音乐播放模式（随机 顺序 单曲）
    void setPlaybackMode(QMediaPlaylist::PlaybackMode mode);
private:
    // 网易云专用网络代理
    void replyMusicInformationDone(QNetworkReply *reply);
    void loadMusicLyrics(MUSIC_INFO musicINFO);
    void replyMusicLyricsDone(QNetworkReply *reply);
    void loadMusicImage(MUSIC_INFO musicINFO);
    void replyMusicImageFinished(QNetworkReply *reply);
private:
    QMediaPlaylist *m_musicList;
    QThread *m_musicThread;
    QMediaPlayer *m_player;
    QTimer *m_lyricsTimer;
    QTimer *m_delayPlayMusicTimer;
    QNetworkAccessManager *m_musicInformationManager;
    QNetworkAccessManager *m_musicLyricsManager;
    QNetworkAccessManager *m_musicIMGManager;
private:
    QList<MUSIC_INFO> m_musicListINFO;
    QList<MUSIC_INFO> m_currentMusicListINFO;
    QList<QPair<QTime, QString>> m_lyrics;
    qint64 m_currentMusicDuration;
    MusicQueryAddress m_musicMusicQueryAddress = MusicQueryAddress::Nothing;
    QString m_localMusicPath;
    QString m_currentNetEaseCloudName;
};

#endif // MUSICPLAYER_H
