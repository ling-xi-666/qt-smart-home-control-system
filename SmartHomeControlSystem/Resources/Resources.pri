HEADERS += \
    $$PWD/MusicPlayer.h \
    $$PWD/ScrollFont.h \
    $$PWD/Weather.h \
    $$PWD/animationlist/mylistwidget.h \
    $$PWD/joystick/joystick.h \
    $$PWD/joystick/platformhead.h \
    $$PWD/progressbar/roundprogressbar.h

SOURCES += \
    $$PWD/MusicPlayer.cpp \
    $$PWD/ScrollFont.cpp \
    $$PWD/Weather.cpp \
    $$PWD/animationlist/mylistwidget.cpp \
    $$PWD/joystick/joystick.cpp \
    $$PWD/progressbar/roundprogressbar.cpp
INCLUDEPATH += $$PWD
