#include "ScrollFont.h"

ScrollFont::ScrollFont(QWidget *parent)
    : QLabel(parent)
{
    m_timer = new QTimer();
    QObject::connect(m_timer, &QTimer::timeout, this, &ScrollFont::updateText);
    m_timer->start(10);

    textColor = QColor(r,r,r);
}

ScrollFont::~ScrollFont()
{
    m_timer->stop();
    delete m_timer;
    m_timer = nullptr;
}

void ScrollFont::setScrollFontText(QString text)
{
    m_text = text;
    m_x = 10;
    m_toLeft = true;
}

void ScrollFont::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setPen(textColor);
    QFont font;
    font.setFamily("Microsoft YaHei");
    font.setPointSize(25);
    font.setBold(true);
    QFontMetrics fm(font);
    int m_width  = fm.horizontalAdvance(m_text);
    painter.setFont(font);
    painter.drawText(m_x, 41, m_text);
    switch (m_moveWay) {
    case MoveWay::LeftToRight:
        m_x += m_step;
        if(m_x > width())m_x = -m_width;
        break;
    case MoveWay::RightToLeft:
        m_x -= m_step;
        if(m_x < -m_width)m_x = width();
        break;
    case MoveWay::LeftAndRight:
        if(m_toLeft){m_x -= m_step;if(m_x < -m_width){m_toLeft = false;}}else{m_x += m_step;if(m_x > width())m_toLeft = true;}
        break;
    default:
        break;
    }
}

void ScrollFont::setMoveWay(ScrollFont::MoveWay way)
{
    m_moveWay = way;
}

void ScrollFont::updateText()
{
    is_rIncrease = r == 255 ? false : (r == 0 ? true : is_rIncrease);
    r = is_rIncrease ? r + 1 : r - 1;
    is_gIncrease = g == 255 ? false : (g == 0 ? true : is_gIncrease);
    g = is_gIncrease ? g + 1 : g - 1;
    is_bIncrease = b == 255 ? false : (b == 0 ? true : is_bIncrease);
    b = is_bIncrease ? b + 1 : b - 1;
    textColor = QColor(r,g,b);
    update();
}
