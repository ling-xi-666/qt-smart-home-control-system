#ifndef SCROLLFONT_H
#define SCROLLFONT_H

#include <QLabel>
#include <QPainter>
#include <QTimer>

class ScrollFont : public QLabel
{
    Q_OBJECT
public:
    ScrollFont(QWidget *parent = 0);
    ~ScrollFont();
public:
    enum MoveWay
    {
        LeftToRight,
        RightToLeft,
        LeftAndRight
    };
    void setScrollFontText(QString test);
    void paintEvent(QPaintEvent *event) override;
    void setMoveWay(MoveWay way);
public slots:
    void updateText();
private:
    bool m_toLeft = true;
    double m_x = 10;
    double m_step = 0.3;
    QString m_text = "NO MUSIC NAME";
    MoveWay m_moveWay = RightToLeft;
    QTimer *m_timer;
    int r = 0, g = 100, b = 200;
    bool is_rIncrease = true;
    bool is_gIncrease = false;
    bool is_bIncrease = true;
    QColor textColor;
};

#endif // SCROLLFONT_H
