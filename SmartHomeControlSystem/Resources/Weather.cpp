#include "Weather.h"

Weather::Weather(QObject *parent) :
    QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
    QObject::connect(m_manager,&QNetworkAccessManager::finished,this,&Weather::replyDone);
}
Weather::~Weather()
{

}

void Weather::setWeather(QString province,QString city)
{
    if (m_area.value("province") == province && m_area.value("city") == city)
        return;
    m_area.insert("province",province);
    m_area.insert("city",city);
    QJsonParseError error;
    QNetworkRequest request;
    QFile jsonFile(":/Resources/jsonCity/city.json");
    jsonFile.open(QIODevice::ReadOnly);
    QJsonArray jsonArray = QJsonDocument::fromJson(jsonFile.readAll(), &error).object().value("cityName").toArray();
    QJsonObject dateObj = jsonArray.at(0).toObject();
    QString cityId;

    for(int i=0; i<jsonArray.size(); i++)
    {
        dateObj = jsonArray.at(i).toObject();
        if(dateObj.value("cityZh").toString().contains(city) && dateObj.value("provinceZh").toString().contains(province))
        {
            cityId = dateObj.value("id").toString();
            request.setUrl(QUrl(QString("http://t.weather.itboy.net/api/weather/city/%1").arg(cityId)));
            request.setRawHeader(QByteArray("User-Agent"), QByteArray("Mozilla/5.0"));
            request.setAttribute(QNetworkRequest::HttpStatusCodeAttribute, true);
            m_manager->get(request);
            break;
        }
    }
    jsonFile.close();
}

QStringList Weather::getAllRegions()
{
    QStringList data;
    QJsonParseError error;
    QFile jsonFile(":/Resources/jsonCity/city.json");
    jsonFile.open(QIODevice::ReadOnly);
    QJsonArray jsonArray = QJsonDocument::fromJson(jsonFile.readAll(), &error).object().value("cityName").toArray();
    jsonFile.close();

    for(int i=0; i<jsonArray.size(); i++)
    {
        data.append(jsonArray.at(i).toObject().value("provinceZh").toString() + " " + jsonArray.at(i).toObject().value("cityZh").toString());
    }
    return data;
}


void Weather::replyDone(QNetworkReply *reply)
{
    QByteArray bytes = reply->readAll();
    QString string = QString::fromUtf8(bytes);
    parseFromJson(string);
}

void Weather::parseFromJson(const QString &jsonStr)
{
    QJsonParseError err;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonStr.toUtf8(), &err);
    QJsonObject jsObj = jsonDoc.object();

    QString message= jsObj.value("message").toString();
    QJsonObject dataObj = jsObj.value("data").toObject();

    WEATHER_INFO info;
    info.high = dataObj.value("yesterday").toObject().value("high").toString();
    info.low = dataObj.value("yesterday").toObject().value("low").toString();
    info.ymd = dataObj.value("yesterday").toObject().value("ymd").toString();
    info.week = dataObj.value("yesterday").toObject().value("week").toString();
    info.sunrise = dataObj.value("yesterday").toObject().value("sunrise").toString();
    info.sunset = dataObj.value("yesterday").toObject().value("sunset").toString();
    info.aqi = dataObj.value("yesterday").toObject().value("aqi").toString();
    info.fx = dataObj.value("yesterday").toObject().value("fx").toString();
    info.fl = dataObj.value("yesterday").toObject().value("fl").toString();
    info.type = dataObj.value("yesterday").toObject().value("type").toString();
    info.fx = dataObj.value("yesterday").toObject().value("fx").toString();
    info.notice = dataObj.value("yesterday").toObject().value("notice").toString();

    m_weather.insert(0,info);

    QJsonArray forecastArr = dataObj.value("forecast").toArray();
    for (int i = 0; i < 6; i++)
    {
        info.high =forecastArr.at(i).toObject().value("high").toString();
        info.low = forecastArr.at(i).toObject().value("low").toString();
        info.ymd = forecastArr.at(i).toObject().value("ymd").toString();
        info.week = forecastArr.at(i).toObject().value("week").toString();
        info.sunrise = forecastArr.at(i).toObject().value("sunrise").toString();
        info.sunset = forecastArr.at(i).toObject().value("sunset").toString();
        info.aqi = forecastArr.at(i).toObject().value("aqi").toString();
        info.fx = forecastArr.at(i).toObject().value("fx").toString();
        info.fl = forecastArr.at(i).toObject().value("fl").toString();
        info.type = forecastArr.at(i).toObject().value("type").toString();
        info.fx = forecastArr.at(i).toObject().value("fx").toString();
        info.notice = forecastArr.at(i).toObject().value("notice").toString();
        m_weather.insert(i + 1,info);
    }
    emit updateWeather(m_weather);
}

