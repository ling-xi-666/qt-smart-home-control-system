#ifndef GETWEATHER_H
#define GETWEATHER_H

#include <QDir>

#include <QEventLoop>

#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

typedef struct
{
    QString cityName;                   // 城市名字
    QString high;                       // 高温
    QString low;                        // 低温
    QString ymd;                        // 日期
    QString week;                       // 星期
    QString sunrise;                    // 日出
    QString sunset;                     // 日落
    QString aqi;                        // aqi
    QString fx;                         // 风向
    QString fl;                         // 风力
    QString type;                       // 天气类型
    QString notice;                     // 提示
}WEATHER_INFO;

class Weather : public QObject
{
    Q_OBJECT
public:
    explicit Weather(QObject *parent = nullptr);
    ~Weather();
public:
    void setWeather(QString province,QString city);
    QStringList getAllRegions();
signals:
    void updateWeather(QList<WEATHER_INFO> info);
private:
    void replyDone(QNetworkReply *reply);
    void parseFromJson(const QString &jsonStr);
private:
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply;

    QList<WEATHER_INFO> m_weather;
    QMap<QString,QString> m_area;
};

#endif // GETWEATHER_H
