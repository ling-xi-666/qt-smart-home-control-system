/**
 ** @author:
 ** @date:
 ** @brief:      版本差异化头文件解决方案
 */

#ifndef PLATFORMHEAD_H
#define PLATFORMHEAD_H

#include <QtGlobal>

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
#  define QT_GREATER_NEW
#else
#  define QT_GREATER_OLD
#endif

#ifdef QT_GREATER_NEW
    #include <QtWidgets>
#else
    #include <QtGui>
#endif

#endif // PLATFORMHEAD_H
