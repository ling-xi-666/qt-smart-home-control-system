#include "HTTPClient.h"

HTTPClient::HTTPClient(QString serverIP)
{
    m_manager = new QNetworkAccessManager(this);
    m_request = new QNetworkRequest();
    m_serverIP = serverIP;
}

HTTPClient::~HTTPClient()
{

}

int HTTPClient::send_HTTP_data(QJsonObject jsonContent)
{
    QByteArray postData;
    jsonContent.insert("serverAccount","123456");
    jsonContent.insert("serverPassword","123456");
    qDebug() << "发送: " << jsonContent;
    postData.append(QJsonDocument(jsonContent).toJson());
    m_request->setUrl(QUrl(m_serverIP));
    m_request->setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/json"));

    // 设置超时处理定时器
    QTimer timer;
    timer.setInterval(10000);
    //    timer->singleShot(3,this,m_reply = m_manager->post(*m_request, postData));
    m_reply = m_manager->post(*m_request, postData);

    QEventLoop eventLoop;
    connect(&timer, &QTimer::timeout, &eventLoop, &QEventLoop::quit);
    connect(m_reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
    timer.start();
    eventLoop.exec();

    if(timer.isActive())
    {
        if(m_reply->error() != QNetworkReply::NoError)
        {
            timer.stop();
            qDebug() << "http请求出错 : " << m_reply->errorString();
            m_reply->deleteLater();
        }
    }
    else
    {
        m_reply = m_manager->post(*m_request, postData);
    }

    qDebug() << "状态码：" << m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).value<int>();
    int statusCode = m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).value<int>();
    emit get_HTTP_data( m_reply->readAll());

    m_reply->abort();
    m_reply->deleteLater();
    m_reply = nullptr;

    return statusCode;
}
