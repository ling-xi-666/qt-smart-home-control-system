#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonDocument>
#include <QTimer>

enum NetworkStatus
{
    OK = 200,
    Forbidden = 403,
    NotFound = 404
};

class HTTPClient : public QObject
{
    Q_OBJECT

public:
    HTTPClient(QString serverIP);
    ~HTTPClient();

Q_SIGNALS:
    void get_HTTP_data(QByteArray data);

public:
    int send_HTTP_data(QJsonObject jsonContent);
private:
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply;
    QNetworkRequest *m_request;
private:
    QString m_serverIP;
};

#endif // HTTPCLIENT_H
