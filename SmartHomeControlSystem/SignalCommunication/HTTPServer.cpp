#include "HTTPServer.h"

HTTPServer::HTTPServer(QObject *parent) : QObject(parent)
{
    m_httpServer = new QTcpServer(this);
    m_httpServer->setMaxPendingConnections(1024);
    connect(m_httpServer,&QTcpServer::newConnection,this,&HTTPServer::newConnection);
}

void HTTPServer::start_HTTP_server(const QHostAddress &address, const quint16 &port)
{
    if (nullptr == m_httpServer)
    {
        m_httpServer = new QTcpServer(this);
        m_httpServer->setMaxPendingConnections(1024);
        connect(m_httpServer,&QTcpServer::newConnection,this,&HTTPServer::newConnection);
    }
    m_httpServer->listen(address,port);
}

void HTTPServer::stop_HTTP_server()
{
    if (nullptr != m_httpServer)
    {
        this->m_httpServer->close();
        delete this->m_httpServer;
        this->m_httpServer = nullptr;
    }
}

void HTTPServer::newConnection()
{
    m_socket = m_httpServer->nextPendingConnection();
    QObject::connect(m_socket,&QTcpSocket::readyRead,this,&HTTPServer::readyRead);
}

void HTTPServer::readyRead()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());
    if(socket)
    {
        QByteArray request = socket->readAll();
        QStringList data = QString(request).split("User-Agent: Mozilla/5.0\r\n\r\n");
        if (data.size() > 1)
        {
            QJsonDocument doc = QJsonDocument::fromJson(QString(data[1]).toUtf8());
            QJsonObject jsonData = doc.object();
            emit get_HTTP_data(data[1].toUtf8());
        }
        else
        {
            QJsonDocument doc = QJsonDocument::fromJson(QString(request).toUtf8());
            QJsonObject jsonData = doc.object();
            emit get_HTTP_data(request);
        }
    }
}

void HTTPServer::send_HTTP_data(bool successOrNot,QByteArray response)
{
    qDebug() << "发送：" << response;
        if (successOrNot)
    {
        QString http = "HTTP/1.1 200 OK\r\n";
        http += "Server: nginx\r\n";
        http += "Content-Type: text/html;charset=utf-8\r\n";
        http += "Connection: keep-alive\r\n";
        http += QString("Content-Length: %1\r\n\r\n").arg(QString::number(response.size()));

        m_socket->write(http.toUtf8());
        m_socket->write(response);
        m_socket->flush();
        m_socket->waitForBytesWritten(http.size() + response.size());
        m_socket->close();
    }
    else
    {
        QString http = "HTTP/1.1 403 Forbidden\r\n";
        http += "Server: nginx\r\n";
        http += "Content-Type: text/html;charset=utf-8\r\n";
        http += "Connection: keep-alive\r\n";
        http += QString("Content-Length: %1\r\n\r\n").arg(QString::number(response.size()));

        m_socket->write(http.toUtf8());
        m_socket->write(response);
        m_socket->flush();
        m_socket->waitForBytesWritten(http.size() + response.size());
        m_socket->close();
    }
}

HTTPServer::~HTTPServer()
{
    if (nullptr != this->m_httpServer)
    {
        this->m_httpServer->close();
        delete this->m_httpServer;
        this->m_httpServer = nullptr;
    }
}
