#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <QObject>
#include <QtNetwork>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QRegExp>

class HTTPServer : public QObject
{
    Q_OBJECT

public:
    explicit HTTPServer(QObject *parent = nullptr);
    ~HTTPServer();

Q_SIGNALS:
    void get_HTTP_data(QByteArray data);

public:

    void start_HTTP_server(const QHostAddress &address = QHostAddress::Any,const quint16 &port = 80);

    void send_HTTP_data(bool successOrNot,QByteArray data);

    void stop_HTTP_server();

private slots:

    void newConnection();
    void readyRead();

private:
    QTcpServer *m_httpServer;
    QTcpSocket *m_socket;
};

#endif // HTTPSERVER_H
