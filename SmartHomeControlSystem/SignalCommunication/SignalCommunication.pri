HEADERS += \
    #$$PWD/BLEClient.h \
    #$$PWD/BLEServer.h \
    #$$PWD/UDPClient.h \
    #$$PWD/UDPServer.h \
    $$PWD/HTTPClient.h \
    $$PWD/HTTPServer.h \
    $$PWD/TCPClient.h \
    $$PWD/TCPServer.h

SOURCES += \
    #$$PWD/BLEClient.cpp \
    #$$PWD/BLEServer.cpp \
    #$$PWD/UDPClient.cpp \
    #$$PWD/UDPServer.cpp \
    $$PWD/HTTPClient.cpp \
    $$PWD/HTTPServer.cpp \
    $$PWD/TCPClient.cpp \
    $$PWD/TCPServer.cpp

INCLUDEPATH += $$PWD
