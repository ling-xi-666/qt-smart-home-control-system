#include "TCPClient.h"

TCPClient::TCPClient()
{
    m_TCPsocket = new QTcpSocket();
    QObject::connect(m_TCPsocket, &QTcpSocket::readyRead, this, &TCPClient::readyRead);
    QObject::connect(m_TCPsocket, &QTcpSocket::disconnected, this, &TCPClient::TCP_connection_disconnected);
}

TCPClient::~TCPClient()
{
    this->m_TCPsocket->close();
    delete this->m_TCPsocket;
    this->m_TCPsocket = nullptr;
}

void TCPClient::start_TCP_client(QString ip,quint16 port)
{
    m_TCPsocket->connectToHost(ip, port);
}

void TCPClient::send_TCP_data(QByteArray data)
{
    m_TCPsocket->write(data);
}

void TCPClient::TCP_connection_disconnected()
{

}

void TCPClient::readyRead()
{
    qDebug() << m_TCPsocket->readAll();
}
