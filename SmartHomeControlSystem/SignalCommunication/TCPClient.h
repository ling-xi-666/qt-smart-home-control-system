#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QTcpSocket>

class TCPClient : public QObject
{
    Q_OBJECT
public:
    explicit TCPClient();
    ~TCPClient();

public:
    void start_TCP_client(QString ip,quint16 port = 80);
    void send_TCP_data(QByteArray data);

private slots:
    void TCP_connection_disconnected();

    void readyRead();

private:
    QTcpSocket *m_TCPsocket;
};
#endif // TCPCLIENT_H
