#include "TCPServer.h"

TCPServer::TCPServer()
{
    m_TCPServer = new QTcpServer(this);
    QObject::connect(m_TCPServer,&QTcpServer::newConnection,this,&TCPServer::readyRead);
    m_TCPSocket = new QTcpSocket(this);
}

TCPServer::~TCPServer()
{
    this->m_TCPSocket->close();
    delete this->m_TCPSocket;
    this->m_TCPSocket = nullptr;
    this->m_TCPServer->close();
    delete this->m_TCPServer;
    this->m_TCPServer = nullptr;
}

void TCPServer::start_TCP_server(const QHostAddress &key,const quint16 &port)
{
    m_TCPServer->listen(key,port);
}

void TCPServer::send_TCP_data(QByteArray data)
{
    m_TCPSocket->write(data);
}

void TCPServer::newConnection()
{
    m_TCPSocket = m_TCPServer->nextPendingConnection();
    qDebug() << m_TCPSocket->peerAddress();
    QObject::connect(m_TCPSocket,&QTcpSocket::readyRead,this,&TCPServer::readyRead);
}

void TCPServer::readyRead()
{
    qDebug() << m_TCPSocket->readAll();
}
