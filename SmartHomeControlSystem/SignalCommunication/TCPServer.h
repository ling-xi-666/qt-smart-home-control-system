#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>

class TCPServer : public QObject
{
    Q_OBJECT

public:
    TCPServer();
    ~TCPServer();

public:
    void start_TCP_server(const QHostAddress &address = QHostAddress::Any,const quint16 &port = 80);

    void send_TCP_data(QByteArray data);
private slots:
    void newConnection();
    void readyRead();

private:
    QTcpServer *m_TCPServer;
    QTcpSocket *m_TCPSocket;
};

#endif // TCPSERVER_H
