#include "SmartHomeControlSystem.h"
#include "ui_SmartHomeControlSystem.h"
SmartHomeControlSystem::SmartHomeControlSystem(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::SmartHomeControlSystem)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinimizeButtonHint);
    this->setStyleSheet("QMainWindow{border-radius: 55px;}");

    m_roundProgressBar = new RoundProgressBar(this);
    m_roundProgressBar->setGeometry(this->width() / 2 - 200 ,this->height() / 2 - 200,100,100);
    m_roundProgressBar->setInnerBarWidth(50);
    m_roundProgressBar->setOutterBarWidth(50);
    m_roundProgressBar->setControlFlags(RoundProgressBar::All);

    m_mainInterface = new MainInterface(this);
    m_mainInterface->setGeometry(0,0,this->width(),this->height());
    QObject::connect(m_mainInterface,&MainInterface::narrowClicked,this,[=]{this->showMinimized();});
    QObject::connect(m_mainInterface,&MainInterface::closeClicked,this,[=]{this->close();});

    m_mainInterface->hide();

    m_powerOnTimer = new QTimer();
    QObject::connect(m_powerOnTimer,&QTimer::timeout,this,[=]{m_powerOnTime++;m_roundProgressBar->setText(m_powerOnTime); if (m_powerOnTime > 100){m_powerOnTimer->stop();m_mainInterface->show();m_roundProgressBar->hide();ui->label->hide();};});
    m_powerOnTimer->start(30);
}

SmartHomeControlSystem::~SmartHomeControlSystem()
{
    m_powerOnTimer->stop();
    delete m_powerOnTimer;
    m_powerOnTimer = nullptr;
    delete ui;
}


void SmartHomeControlSystem::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton)
    {
        is_drag_ = false;
    }
}


void SmartHomeControlSystem::mousePressEvent(QMouseEvent * e)
{
    if(e->button() == Qt::LeftButton && e->pos().y() >= 0 && e->pos().y() < 50)
    {
        is_drag_ = true;
    }
    window_start_point_ = e->globalPos() - this->pos();
}

void SmartHomeControlSystem::mouseMoveEvent(QMouseEvent *e)
{
    if(is_drag_ && e->pos().y() >= 0 && e->pos().y() < 50)
    {
        mouse_start_point_ = e->globalPos();
        this->move(mouse_start_point_ - window_start_point_);
    }
}
