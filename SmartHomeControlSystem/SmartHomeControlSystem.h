#ifndef SMARTHOMECONTROLSYSTEM_H
#define SMARTHOMECONTROLSYSTEM_H

#include <QMainWindow>
#include <QMouseEvent>

#include "progressbar/roundprogressbar.h"
#include "MainInterface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SmartHomeControlSystem; }
QT_END_NAMESPACE

class SmartHomeControlSystem : public QMainWindow
{
    Q_OBJECT

public:
    SmartHomeControlSystem(QWidget *parent = nullptr);
    ~SmartHomeControlSystem();

protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

private:
    Ui::SmartHomeControlSystem *ui;

    RoundProgressBar *m_roundProgressBar;
    MainInterface *m_mainInterface;

    bool is_drag_ = false;
    QPoint mouse_start_point_;
    QPoint window_start_point_;

    QTimer *m_powerOnTimer;
    int m_powerOnTime = 0;
};
#endif // SMARTHOMECONTROLSYSTEM_H
