#include "SmartHomeControlSystem.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStringList fontFamilies = QFontDatabase::applicationFontFamilies(QFontDatabase::addApplicationFont(QStringLiteral(":/fonts/hanyiyongzidongganguangbo.ttf")));
    if (fontFamilies.size() > 0)
    {
        QFont font;
        font.setFamily(fontFamilies[0]);
        a.setFont(font);
    }
    SmartHomeControlSystem w;
    w.show();
    return a.exec();
}
